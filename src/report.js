const Apify = require('apify');

// format doesn't work with padStart on Slack
const exportConsolidatedStats = (statsByLabel, trackOutputs) => Object
    .entries(statsByLabel)
    .sort(([, stats1], [, stats2]) => stats1.ratio - stats2.ratio)
    .map(([target, stats]) => (trackOutputs.length ?
        `\`${stats.ratio.toString().padEnd(3, ' ')}%\` [${trackOutputs.map(output => `${output}: \`${stats.outputs[output].ratio.toString().padEnd(3, ' ')}%\``)}] *${target}* \`${stats.success}/${stats.total}\`` :
        `\`${stats.ratio.toString().padEnd(3, ' ')}%\` | *${target}* \`${stats.success}/${stats.total}\``));

const getConsolidatedContent = (stats, trackOutputs) => `
----
:dart: Targets:
----
${exportConsolidatedStats(stats.consolidated, trackOutputs).join('\n')}`;

const getDetailsDatasetContent = errorDataset => `
----
:face_with_monocle: Details: https://my.apify.com/storage/dataset/${process.env.APIFY_DEFAULT_DATASET_ID}`;

const getReportMessageContent = ({consolidateBy, trackOutputs, errorDataset, isWeeklyReport, stats}) => {
    const {success, failure, other, total} = isWeeklyReport ? stats.weekly : stats.today;

    return `
:bell: ${isWeeklyReport ? 'WEEKLY' : 'DAILY'} STATS
----
:heavy_check_mark: Success: ${success},
:x: Failure: ${failure},
:grey_question: Other: ${other},
:shopping_trolley: Total: ${total},
----
:popcorn: Ratio: ${Math.floor((success / (total || 1)) * 100)}% ${consolidateBy ? getConsolidatedContent(stats, trackOutputs) : ''} ${errorDataset ? getDetailsDatasetContent(errorDataset) : ''}
`;
};

const exportErrorsToDataset = async (actorId, recentRuns, pivot) => {
    const recentFailedRuns = recentRuns.filter(run => run.status !== 'SUCCEEDED');

    const recentFailedRunsOutput = recentFailedRuns.map(run => ({
        url: `https://my.apify.com/view/runs/${run.id}`,
        [pivot]: run.input && run.input.value[pivot],
        status: run.status,
        origin: run.meta.origin,
        started: run.startedAt,
        finished: run.finishedAt,
        duration: run.finishedAt && ((run.finishedAt - run.startedAt) / 1000).toFixed(2),
    }));

    const dataset = await Apify.openDataset(`${actorId}-monitoring`);
    await dataset.pushData(recentFailedRunsOutput);
    await Apify.pushData(recentFailedRunsOutput);
};

module.exports = {
    getReportMessageContent,
    exportErrorsToDataset,
};
