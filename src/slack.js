const Slack = require('slack-node');

const throttleNotification = timestamp => {
    const currentTimestamp = Date.now();
    const oldDate = new Date(timestamp);
    const newDate = new Date(currentTimestamp);
    const oldDayNumber = oldDate.getDate();
    const newDayNumber = newDate.getDate();
    const skipNotification = newDayNumber === oldDayNumber;

    return {
        oldDayNumber,
        skipNotification,
    };
};

const _notifySlackBound = async (context, message, error = null) => {
    const {store, slackToken, simulateOnly, slackChannels, slackMentions, slackTimestamp} = context;

    if (slackTimestamp) {
        const {oldDayNumber, skipNotification} = throttleNotification(slackTimestamp);

        if (skipNotification) {
            console.log(`Notification skipped - already sent on day #${oldDayNumber}`);
            return;
        }
    }

    const slack = new Slack(slackToken);
    const textWithMentions = `${slackMentions.map(slackUserID => `<@${slackUserID}>`).join(' ')} ${message}`.trim();
    await Promise.all(slackChannels.map(channel => postMessage({slack, channel, simulateOnly, slackMentions, textWithMentions})));
    await store.setValue('slackTimestamp', Date.now());
};

const notifySlackBound = context => _notifySlackBound.bind(null, context);

async function postMessage({slack, channel, simulateOnly, slackMentions: mentionSlackUserIDs, textWithMentions: text}) {
    console.log('Dispatching Slack notification:', {
        text,
        channel,
    });

    if (simulateOnly)
        return Promise.resolve(null);

    return new Promise((resolve, reject) => {
        slack.api('chat.postMessage', {
            text,
            channel,
        }, (err, response) => {
            if (err)
                reject(err);
            else
                resolve(response);
        });
    });
}

module.exports = {
    notifySlackBound,
};
