const getListUntilMatch = async (apifyClient, actorId, matchIndex, itemsCache = [], runsList = {}) => {
    const options = {
        desc: true,
    };

    if (runsList.limit)
        options.offset = runsList.offset + runsList.limit;

    runsList = await apifyClient.actor(actorId).runs().list(options);
    const {items, count, total, limit, offset} = runsList;
    const index = items.findIndex(matchIndex);
    const matchFound = !!~index;
    const loadMore = !matchFound && total > count + offset;

    itemsCache = itemsCache
        .concat(matchFound ?
            items.slice(0, index) :
            items);

    if (loadMore)
        return getListUntilMatch(apifyClient, actorId, matchIndex, itemsCache, runsList);

    return itemsCache;
};

module.exports = {
    getListUntilMatch,
};
