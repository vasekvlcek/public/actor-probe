const extendActorRunsWith = {
    input: async (actorRuns, apifyClient) =>
        Promise.all(actorRuns.map(async run => ({
            ...run,
            input: await apifyClient.keyValueStore(run.defaultKeyValueStoreId).getRecord('INPUT') || {},
        }))),
    output: async (actorRuns, apifyClient) =>
        Promise.all(actorRuns.map(async run => ({
            ...run,
            output: await apifyClient.keyValueStore(run.defaultKeyValueStoreId).getRecord('OUTPUT') || {},
        }))),
};

const getExtendedActorRuns = async (actorRuns, apifyClient, trackOutputs) => {
    const actorRunsWithInput = await extendActorRunsWith.input(actorRuns, apifyClient);

    if (trackOutputs.length)
        return extendActorRunsWith.output(actorRunsWithInput, apifyClient);
        // stats.outputs = getStatsForOutputs(actorRunsWithOutput, trackOutputs);

    return actorRunsWithInput;
};

const getActorRunsByLabel = (actorRuns, consolidateBy) =>
    actorRuns.reduce((pool, next) => ({
        ...pool,
        [next.input.value[consolidateBy]]: [...pool[next.input.value[consolidateBy]] || [], next],
    }), {});

const getStatsForOutputs = (actorRuns, trackOutputs) => trackOutputs.reduce((pool, next) => {
    const count = actorRuns.filter(run => run.output.value && run.output.value[next]).length;

    return {
        ...pool,
        [next]: {
            count,
            ratio: Math.floor((count / (actorRuns.length || 1)) * 100),
        },
    };
}, {});

const getConsolidatedStats = async (actorRuns, apifyClient, consolidateBy, trackOutputs) => {
    actorRuns = await getExtendedActorRuns(actorRuns, apifyClient, trackOutputs);
    const actorRunsByLabel = getActorRunsByLabel(actorRuns, consolidateBy);

    return Object.entries(actorRunsByLabel).reduce(async (asyncPool, [label, runs]) => {
        const pool = await asyncPool;
        const success = runs.filter(run => run.status.toLowerCase() === 'succeeded').length;
        const failure = runs.filter(run => run.status.toLowerCase() === 'failed').length;
        const other = runs.length - success - failure;

        const stats = {
            ...pool,
            [label]: {
                success,
                failure,
                other,
                total: runs.length,
                ratio: Math.floor((success / (runs.length || 1)) * 100),
            },
        };

        if (trackOutputs.length)
            stats[label].outputs = getStatsForOutputs(runs, trackOutputs);

        return stats;
    }, Promise.resolve({}));
};

module.exports = {
    getConsolidatedStats,
};
