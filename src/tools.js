const Apify = require('apify');

const saveContent = async page => Apify.setValue('html', await page.content(), {contentType: 'text/html'});

const saveDebugBuffers = async (page, name = 'error') => {
    try {
        const screenshotBuffer = await page.screenshot();
        await Apify.setValue('screen', screenshotBuffer, { contentType: 'image/png' });
    } catch (error) {
        console.log('Failed to take a screenshot');
    }

    try {
        const html = await page.evaluate('document.documentElement.outerHTML');
        await Apify.setValue('html', html, {contentType: 'text/html'});
    } catch (error) {
        console.log('Failed to grab HTML');
    }
};

module.exports = {
    saveContent,
    saveDebugBuffers,
};
