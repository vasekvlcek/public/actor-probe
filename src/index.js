const Apify = require('apify');
const ApifyClient = require('apify-client');

const apifyClient = new ApifyClient({token: process.env.THIRD_PARTY_APIFY_TOKEN || process.env.APIFY_TOKEN});

const {getListUntilMatch} = require('./client');
const {getConsolidatedStats} = require('./stats');
const {getReportMessageContent} = require('./report');
const {exportErrorsToDataset} = require('./report');
const {notifySlackBound} = require('./slack');

Apify.main(async () => {
    const {
        actorId,
        consolidateBy,
        errorDataset,
        calendarDays,
        simulateOnly,
        trackOutputs,
        slackToken = process.env.SLACK_TOKEN,
        slackChannels,
        slackMentions = [],
    } = await Apify.getInput() || require('../INPUT_LOCAL');

    if (typeof actorId !== 'string')
        throw Error('ID of actor to be monitored must be provided on input!');

    if (!Array.isArray(slackChannels))
        throw Error('Array of Slack channel names must be provided on input!');

    if (slackMentions && !Array.isArray(slackMentions))
        throw Error('Array of Slack user IDs must be provided on input!');

    if (typeof slackToken !== 'string')
        throw Error('Slack access token for notifications must be provided in environment variable SLACK_TOKEN!');

    if (trackOutputs && !consolidateBy)
        console.log('Consolidated results were not requested. Ignoring option [trackOutputs]');

    const store = await Apify.openKeyValueStore(`${actorId}-monitoring`);
    const currentTimestamp = Date.now();
    const currentDate = new Date(currentTimestamp);
    const recentStartDate = new Date(new Date(currentDate).setHours(currentDate.getHours() - 24));
    const weeklyStartDate = new Date(new Date(currentDate).setDate(currentDate.getDate() - 7));
    const isWeeklyReport = new Date(currentTimestamp).getDay() === 0;
    const slackTimestamp = await store.getValue('slackTimestamp');
    const lastReportDate = await store.getValue('lastReportDate') || {};

    console.log({lastReportDate});

    const notifySlack = notifySlackBound({store, slackToken, simulateOnly, slackChannels, slackMentions, slackTimestamp});

    const stats = {};

    if (!isWeeklyReport) {
        const matchIndex = calendarDays ?
            (input, index, array) => input.startedAt.toDateString() !== array[0].startedAt.toDateString() :
            recentStartDate => (input, index, array) => input.startedAt < recentStartDate;

        const recentAndActiveRuns = await getListUntilMatch(apifyClient, actorId, matchIndex(recentStartDate));
        // const recentRuns = recentAndActiveRuns.filter(run => run.status !== 'RUNNING').slice(0, 10);
        const recentRuns = recentAndActiveRuns.filter(run => run.status !== 'RUNNING');

        const [lastRun] = recentRuns;
        const recentDate = {
            run: lastRun && lastRun.startedAt,
            report: new Date(lastReportDate.recent || null),
        };

        console.log({recentDate});

        if (!lastRun || recentDate.run <= recentDate.report) {
            console.log('No new runs found since last report');
            return;
        }

        if (recentDate.run)
            lastReportDate.recent = recentDate.run;

        const recentCounts = {
            success: recentRuns.filter(run => run.status.toLowerCase() === 'succeeded').length,
            failure: recentRuns.filter(run => run.status.toLowerCase() === 'failed').length,
        };

        recentCounts.other = recentRuns.length - recentCounts.success - recentCounts.failure;
        recentCounts.total = recentRuns.length;

        stats.today = recentCounts;

        if (consolidateBy)
            stats.consolidated = await getConsolidatedStats(recentRuns, apifyClient, consolidateBy, trackOutputs);

        if (errorDataset && Apify.isAtHome())
            await exportErrorsToDataset(actorId, recentRuns, consolidateBy);
    }

    if (isWeeklyReport) {
        const matchIndex = input => input.startedAt < weeklyStartDate;
        const weeklyAndActiveRuns = await getListUntilMatch(apifyClient, actorId, matchIndex);
        const weeklyRuns = weeklyAndActiveRuns.filter(run => run.status !== 'RUNNING');

        const [lastRun] = weeklyRuns;
        const recentDate = {
            run: lastRun && lastRun.startedAt,
            report: new Date(lastReportDate.weekly || null),
        };

        console.log({recentDate});

        if (!lastRun || recentDate.run <= recentDate.report) {
            console.log('No new runs found since last report');
            return;
        }

        if (recentDate.run)
            lastReportDate.weekly = lastReportDate.recent = recentDate.run;

        const weeklyCounts = {
            success: weeklyRuns.filter(run => run.status.toLowerCase() === 'succeeded').length,
            failure: weeklyRuns.filter(run => run.status.toLowerCase() === 'failed').length,
        };

        weeklyCounts.other = weeklyRuns.length - weeklyCounts.success - weeklyCounts.failure;
        weeklyCounts.total = weeklyRuns.length;

        stats.weekly = weeklyCounts;

        if (consolidateBy)
            stats.consolidated = await getConsolidatedStats(weeklyRuns, apifyClient, consolidateBy, trackOutputs);

        if (errorDataset && Apify.isAtHome())
            await exportErrorsToDataset(actorId, weeklyRuns, consolidateBy);
    }

    const message = getReportMessageContent({consolidateBy, trackOutputs, errorDataset, isWeeklyReport, stats});

    await notifySlack(message.trim());
    await store.setValue('lastReportDate', lastReportDate);
    await Apify.setValue('OUTPUT', message, {contentType: 'text/plain'});
});
