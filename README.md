## Actor Probe

Reports performance stats of any actor calculated from its runs on a daily and weekly basis. Notification will be skipped if no new runs are found since the last report.

**Access 3rd party actors**

Define secret environment variable to access other users' actors: `THIRD_PARTY_APIFY_TOKEN`

## Prerequisites
| Field | Type | Optional | Description |
| ----- | ---- | -------- | ----------- |
|SLACK_TOKEN|String|no|Environment variable holding Slack token

## INPUT
| Field | Type | Optional | Description |
| ----- | ---- | -------- | ----------- |
|Actor ID|String|no|Report performance stats for this actor
|Consolidate by|Array|no|Consolidate statistics based on some input value of the target runs
|Track outputs|Array|yes|Report occurence ratio of output match across runs for specific run outputs
|Slack channels|Array|no|Slack channels to send the report
|Slack mentions|Array|yes|Slack users to notify in the report
|Calendar days|Boolean|yes|Use calendar days for daily checks instead of fixed 24-hour periods
|Simulate only|Boolean|yes|Do not really dispatch the notification and mark its last timestamp (dry run)
|Error dataset|Boolean|yes|Export failed runs and link to probe's default dataset from the notification

## OUTPUT

Dispatches notification to specified Slack channels and optionally notifies users specified on input.
